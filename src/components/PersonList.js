import React, { useEffect, useState } from "react";
import PersonCard from "./PersonCard";
import { firestore } from "../index";
import "./css/PersonList.css";
import { Divider } from "antd";

const PersonList = (props) => {
  const [persons, setPersons] = useState([]);
  const getFirestoreData = () => {
    firestore.collection("persons").onSnapshot((snapshot) => {
      let personData = snapshot.docs.map((person) => {
        const {
          first_name,
          last_name,
          nick_name,
          email,
          tel,
          univ,
          address,
        } = person.data();
        return {
          first_name,
          last_name,
          nick_name,
          email,
          tel,
          univ,
          address,
          id: person.id,
        };
      });
      setPersons(personData);
    });
  };
  useEffect(() => {
    getFirestoreData();
  }, []);
  if (persons.length === 0) {
    return (
    <div id="anouce">
      <p>You never add any friend</p>
    </div>
    )
  }
  return (
    <div>
      <div className="outer-border">
        <p id="head">
          <Divider>All of My Friends</Divider>
        </p>
        <div className="list-view-container">
          {persons.map((person, idx) => (
            <div>
              <PersonCard {...person} />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default PersonList;
