import React, { useState, useEffect } from "react";
import "./css/friendlist.css";
import { firestore } from "../index";
import { Table } from "antd";
const FriendList = () => {
  const [persons, setPersons] = useState([]);
  const [friendsCount, setFriendsCount] = useState(0);
  const getFirestoreData = async () => {
    firestore.collection("persons").onSnapshot((snapshot) => {
      let personData = snapshot.docs.map((person) => {
        const {
          first_name,
          last_name,
          nick_name,
          email,
          tel,
          univ,
          address,
        } = person.data();
        return {
          first_name,
          last_name,
          nick_name,
          email,
          tel,
          univ,
          address,
          id: person.id,
        };
      });
      setPersons(personData);
      setFriendsCount(snapshot.docs.length);
    });
  };

  useEffect(() => {
    getFirestoreData();
  }, []);
  const columns = [
    {
      title: "First Name",
      dataIndex: "first_name",
      key: "first_name",
    },
    {
      title: "Last Name",
      dataIndex: "last_name",
      key: "last_name",
    },
    {
      title: "Nickname",
      dataIndex: "nick_name",
      key: "nick_name",
    },
    {
      title: "E-Mail",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Tel.",
      dataIndex: "tel",
      key: "tel",
    },
  ];

  return (
    <div className="Table-margin">
      <p id="table-head">Friends List</p>
      <p>There are {friendsCount} friends in database</p>
      <Table dataSource={persons} columns={columns} />;
    </div>
  );
};
export default FriendList;
