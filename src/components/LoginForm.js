import React, { useState } from "react";
import { Input, Button, Row, Col } from "antd";
import "./css/Login.css";
import axios from "axios";
import { notification } from "antd";
const LoginForm = (props) => {
  const [user, setUser] = useState("");
  const [password, setPassword] = useState("");
  const openNotification = () => {
    notification.open({
      message: "Something went wrong",
      description:
        "Cannot login maybe you have entered invalid PSU Passport or password",
    });
  };
  
  return (
    <div>
      <Banner/>
      <div className="Form">
        <div className="Form-input">
          <p>
            <h2 id="head">Login</h2>
          </p>
          <p>
            <Input
              placeholder="PSU-PASSPORT"
              onChange={(e) => {
                setUser(e.target.value);
              }}
            />
          </p>
          <p>
            <Input
              placeholder="Password"
              type="password"
              onChange={(e) => {
                setPassword(e.target.value);
              }}
            />
          </p>
          <p>
            <div className="btn-box">
              <div className="col">
                <Row gutter={12}>
                  <Col>
                    <Button
                      type="primary"
                      onClick={() => {
                        axios
                          .post(`https://dcw-mini-friendstore.herokuapp.com/`, {
                            username: user,
                            password: password,
                          })
                          .then((res) => {
                            let userData = {
                              st_code: res.data.GetStaffDetailsResult.string[0],
                              f_name: res.data.GetStaffDetailsResult.string[1],
                              l_name: res.data.GetStaffDetailsResult.string[2],
                              p_id: res.data.GetStaffDetailsResult.string[3],
                            };
                            if (
                              userData.st_code !== "" &&
                              userData.f_name !== "" &&
                              userData.l_name !== "" &&
                              userData.p_id !== ""
                            ) {
                              console.log(userData);
                              let jsonData = JSON.stringify(userData);
                              localStorage.setItem("user", jsonData);
                              props.history.push("/");
                            } else {
                              openNotification();
                            }
                          });
                      }}
                    >
                      Login
                    </Button>{" "}
                  </Col>
                </Row>
              </div>
            </div>
          </p>
        </div>
      </div>
      <br/>
      <div id="sign">
        <p>Develop By : Donnukrit Satirakul</p>
        <p>5935512007</p>
        <p>240-311 DCW</p>
      </div>

    </div>
  );
};
export default LoginForm;

const Banner = () => {
  return (
    <div className="banner">
      <p id="banner-text">
        Friendstore
      </p>
    </div>
  )
};
