import React from "react";
import "./css/MenuComponents.css";
import { Row, Col, Button, } from "antd";
import {withRouter} from 'react-router-dom'
const TopBar = (props) => {
let {st_code,f_name,l_name} = props.userData
const Logoff = () => {
    localStorage.removeItem("user")
    props.history.push("/login")
}
  return (
    <div className="top-bar">
        <Row>
            <Col span={5} id="centered">
                <div className="col-4 left"><h1 id="header">Friend Store</h1></div>
            </Col>
            <Col span={19}>
                <div className="menu">
                    <div className="col-4 right menu-item items"><a href="/">Friend Board</a></div>
                    <div className="col-4 right menu-item items"><a href="/friendlist">Friendlist</a></div>
                    <div className="col-4 right menu-item items"><a href="/create">Create Record</a></div>
                    <div className="col-4 right menu-item items"><a href="https://kalimapz.netlify.app/">Contact Me</a></div>
                    <div className="col-4 right menu-item items">Loggin As {st_code} {f_name} {l_name}</div>
                    <div className="col-4 right menu-item items"><Button type="danger" onClick={()=>{Logoff()}}>Logout</Button></div>
                </div>
            </Col>
        </Row>
    </div>
  );
};

export default withRouter(TopBar)