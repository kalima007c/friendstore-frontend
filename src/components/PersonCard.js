import React from "react";
import { Card, Button } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { useDispatch } from "react-redux";
import "./css/card.css";
import { firestore } from "../index";
import { withRouter } from "react-router-dom";

const PersonCard = (props) => {
  const { first_name, last_name, nick_name, email, tel, univ, address } = props;
  const dispatch = useDispatch();
  const deletePerson = (id) => {
    firestore.collection("persons").doc(props.id).delete();
    dispatch({ type: "DELETE_PERSON", id: props.id });
  };
  return (
    <Card id="p-card" title={first_name + " " + last_name}>
      <p id="nick-name">
        <b>{nick_name}</b>
      </p>
      <p id="univ">
        <b>{univ}</b>
      </p>
      <p>Tel. {tel}</p>
      <p>Email : {email}</p>
      <p>
        {address.home_id} , {address.building} , {address.street}
      </p>
      <p>
        {address.district} , {address.province} , {address.zip_code}
      </p>
      <p id="del-btn">
        <Button
          icon={<EditOutlined />}
          onClick={() => {
            props.history.push("/edit/" + props.id);
          }}
        />{" "}
        <Button
          icon={<DeleteOutlined />}
          type="danger"
          onClick={() => {
            deletePerson();
          }}
        />
      </p>
    </Card>
  );
};

export default withRouter(PersonCard);
