import React, { useEffect } from "react";
import { Input, Row, Col, Divider, Button, notification } from "antd";
import "./css/Form.css";
import { useDispatch, useSelector } from "react-redux";
import { firestore } from "../index";

const EditForm = (props) => {
  const dispatch = useDispatch();
  const form = useSelector((state) => state.form);
  const popupNotification = (type) => {
    notification[type]({
      message: "Updated",
      description: "Your data has been updated.",
    });
  };
  const updateData = () => {
    firestore
      .collection("persons")
      .doc(props.match.params.persons_id)
      .set(form);
    popupNotification("success");
  };
  const fetchData = () => {
    firestore
      .collection("persons")
      .doc(props.match.params.persons_id)
      .get()
      .then((person) => {
        dispatch({
          type: "CHANGE_FIRST_NAME",
          first_name: person.data().first_name,
        });
        dispatch({
          type: "CHANGE_LAST_NAME",
          last_name: person.data().last_name,
        });
        dispatch({
          type: "CHANGE_NICK_NAME",
          nick_name: person.data().nick_name,
        });
        dispatch({
          type: "CHANGE_EMAIL",
          email: person.data().email,
        });
        dispatch({
          type: "CHANGE_TEL",
          tel: person.data().tel,
        });
        dispatch({
          type: "CHANGE_UNIVERSITY",
          university: person.data().university,
        });

        dispatch({
          type: "CHANGE_HOME_ID",
          home_id: person.data().address.home_id,
        });
        dispatch({
          type: "CHANGE_BUILDING",
          building: person.data().address.building,
        });
        dispatch({
          type: "CHANGE_STREET",
          street: person.data().address.street,
        });
        dispatch({
          type: "CHANGE_DISTRICT",
          district: person.data().address.district,
        });
        dispatch({
          type: "CHANGE_PROVINCE",
          province: person.data().address.province,
        });
        dispatch({
          type: "CHANGE_ZIPCODE",
          zip_code: person.data().address.zip_code,
        });
      });
  }
  
  useEffect(() => {
    fetchData()
    // eslint-disable-next-line
  }, []);

  return (
    <div className="outer-form">
      <div className="input-form">
        <p>
          <Divider>Edit Information</Divider>
        </p>
        <p>
          <Row gutter={10}>
            <Col span={12}>
              <Input
                value={form.first_name}
                placeholder="First Name"
                onChange={(e) =>
                  dispatch({
                    type: "CHANGE_FIRST_NAME",
                    first_name: e.target.value,
                  })
                }
              />
            </Col>
            <Col span={12}>
              <Input
                value={form.last_name}
                placeholder="Last Name"
                onChange={(e) =>
                  dispatch({
                    type: "CHANGE_LAST_NAME",
                    last_name: e.target.value,
                  })
                }
              />
            </Col>
          </Row>
        </p>
        <p>
          <Input
            value={form.nick_name}
            placeholder="Nickname"
            onChange={(e) =>
              dispatch({
                type: "CHANGE_NICK_NAME",
                nick_name: e.target.value,
              })
            }
          />
        </p>
        <p>
          <Input
            value={form.email}
            placeholder="E-Mail"
            onChange={(e) =>
              dispatch({
                type: "CHANGE_EMAIL",
                email: e.target.value,
              })
            }
          />
        </p>
        <p>
          <Input
            value={form.tel}
            placeholder="Tel"
            onChange={(e) =>
              dispatch({
                type: "CHANGE_TEL",
                tel: e.target.value,
              })
            }
          />
        </p>
        <p>
          <Input
            value={form.university}
            placeholder="University"
            onChange={(e) =>
              dispatch({
                type: "CHANGE_UNIVERSITY",
                university: e.target.value,
              })
            }
          />
        </p>
        <p>
          <Divider orientation="left">Address</Divider>
        </p>
        <p>
          <Row gutter={10}>
            <Col>
              <Input
                value={form.address.home_id}
                placeholder="House No."
                onChange={(e) =>
                  dispatch({
                    type: "CHANGE_HOME_ID",
                    home_id: e.target.value,
                  })
                }
              />
            </Col>
            <Col>
              <Input
                value={form.address.building}
                placeholder="Building"
                onChange={(e) =>
                  dispatch({
                    type: "CHANGE_BUILDING",
                    building: e.target.value,
                  })
                }
              />
            </Col>
          </Row>
        </p>
        <p>
          <Row gutter={10}>
            <Col span={12}>
              <Input
                value={form.address.street}
                placeholder="Street"
                onChange={(e) =>
                  dispatch({
                    type: "CHANGE_STREET",
                    street: e.target.value,
                  })
                }
              />
            </Col>
            <Col span={12}>
              <Input
                value={form.address.district}
                placeholder="District"
                onChange={(e) =>
                  dispatch({
                    type: "CHANGE_DISTRICT",
                    district: e.target.value,
                  })
                }
              />
            </Col>
          </Row>
        </p>
        <p>
          <Row gutter={10}>
            <Col span={12}>
              <Input
                value={form.address.province}
                placeholder="Province"
                onChange={(e) =>
                  dispatch({
                    type: "CHANGE_PROVINCE",
                    province: e.target.value,
                  })
                }
              />
            </Col>
            <Col span={12}>
              <Input
                value={form.address.zip_code}
                placeholder="Zipcode"
                onChange={(e) =>
                  dispatch({
                    type: "CHANGE_ZIPCODE",
                    zip_code: e.target.value,
                  })
                }
              />
            </Col>
          </Row>
        </p>
        <p id="button-col">
          <Button type="primary" onClick={() => updateData()}>
            Edit
          </Button>{" "}
        </p>
      </div>
    </div>
  );
};

export default EditForm;
