import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import PersonForm from "./PersonForm";
import TopBar from "./TopBar";
import PersonList from "./PersonList";
import FriendList from "./FriendList";
import EditForm from "./EditForm";

let user = localStorage.getItem("user")
let userJson = JSON.parse(user)
const SiteRender = () => {

  if (user)
    return (
      <div className="views">
        <Switch>
          <Route exact path="/" render={()=><TopBar userData={userJson}/>}/>
          <Route path="/create" render={()=><TopBar userData={userJson}/>}/>
          <Route path="/friendlist" render={()=><TopBar userData={userJson}/>} />
          <Route path="/edit/:persons_id" render={()=><TopBar userData={userJson}/>} />
        </Switch>
        <Switch>
          <Route exact path="/" component={PersonList} />
          <Route path="/create" component={PersonForm} />
          <Route path="/friendlist" component={FriendList} />
          <Route path="/edit/:persons_id" component={EditForm} />
        </Switch>
       
      </div>
    );
  else {
    return <Redirect to="/login"></Redirect>;
  }
};
export default SiteRender;
