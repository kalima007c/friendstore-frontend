import { createStore, combineReducers } from "redux";
const initialForm = {
  first_name: "",
  last_name: "",
  nick_name: "",
  tel: "",
  email: "",
  university: "",
  address: {
    home_id: "",
    building: "",
    street: "",
    district: "",
    province: "",
    zip_code: "",
  },
};
const formReducer = (data = initialForm, action) => {
  switch (action.type) {
    case "CHANGE_FIRST_NAME":
      return { ...data, first_name: action.first_name };
    case "CHANGE_LAST_NAME":
      return { ...data, last_name: action.last_name };
    case "CHANGE_NICK_NAME":
      return { ...data, nick_name: action.nick_name };
    case "CHANGE_EMAIL":
      return { ...data, email: action.email };
    case "CHANGE_TEL":
      return { ...data, tel: action.tel };
    case "CHANGE_UNIVERSITY":
      return { ...data, university: action.university };
    case "CHANGE_HOME_ID":
      return { ...data, address: { ...data.address, home_id: action.home_id } };
    case "CHANGE_BUILDING":
      return {
        ...data,
        address: { ...data.address, building: action.building },
      };
    case "CHANGE_STREET":
      return { ...data, address: { ...data.address, street: action.street } };
    case "CHANGE_DISTRICT":
      return {
        ...data,
        address: { ...data.address, district: action.district },
      };
    case "CHANGE_PROVINCE":
      return {
        ...data,
        address: { ...data.address, province: action.province },
      };
    case "CHANGE_ZIPCODE":
      return {
        ...data,
        address: { ...data.address, zip_code: action.zip_code },
      };
    default: ///
      return data;
  }
};
const personReducer = (persons = [], action) => {
  switch (action.type) {
    case "ADD_PERSONS":
      return [...persons, action.person];
    case "DELETE_PERSON":
      return persons.filter((person) => +person.id !== +action.id);
    default:
      break;
  }
  return persons;
};

const rootReducer = combineReducers({
  person: personReducer,
  form: formReducer,
});

export const store = createStore(rootReducer);
